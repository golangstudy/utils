/*
@Time : 2018/6/14 上午10:45
@Author : tengjufeng
@File : excelutil
@Software: GoLand
*/

package main

import (
	"encoding/json"
	"fmt"
	"github.com/tealeg/xlsx"
	"net/http"
)

func excel(w http.ResponseWriter, r *http.Request) {
	//w.Header().Set("Access-Control-Allow-Origin", "*")             //允许访问所有域
	//w.Header().Add("Access-Control-Allow-Headers", "Content-Type") //header的类型
	w.Header().Set("content-type", "application/json") //返回数据格式是json
	//w.Header().Set("content-type", "image/png") //返回数据格式是json
	//w.Header().Set("Content-Disposition", "attachmen")
	//w.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")

	r.ParseForm() //解析参数，默认是不会解析的
	readExcel(w)
	//downExcel(w, r)
	//test(w)
	//http.ServeFile(w, r, "./test.xlsx")
}

func test(w http.ResponseWriter) {
	type Profile struct {
		Name    string   `json:"name"`
		Hobbies []string `json:"hobbies"`
	}
	profile := Profile{"SuperWang", []string{"football", "programming"}}

	js, err := json.Marshal(profile)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
func readExcel(w http.ResponseWriter) {
	excelFilename := "./test.xlsx"
	xlfile, err := xlsx.OpenFile(excelFilename)
	if err != nil {
		fmt.Println("xlsx read file error : ", err)
	}
	type mapstruct struct {
		Name  string `json:"name"`
		Phone string `json:"phone"`
	}
	maps := []mapstruct{}
	ma := mapstruct{}
	for _, sheet := range xlfile.Sheets {
		fmt.Println("sheet name = ", sheet.Name)
		for _, row := range sheet.Rows {

			for key, cell := range row.Cells {
				switch key {
				case 0:
					ma.Name = cell.String()
				case 1:
					ma.Phone = cell.String()
				}
				fmt.Println(key, cell)
			}
			maps = append(maps, ma)
		}
	}

	w.Header().Set("Content-Type", "application/json")

	jj, _ := json.Marshal(maps)

	w.Write(jj)
}

func downExcel(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Disposition", "attachmen")
	w.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")

	var file *xlsx.File
	var sheet *xlsx.Sheet
	var row, row1, row2 *xlsx.Row
	var cell *xlsx.Cell
	var err error

	file = xlsx.NewFile()
	sheet, err = file.AddSheet("Sheet1")
	if err != nil {
		fmt.Printf(err.Error())
	}
	row = sheet.AddRow()
	row.SetHeightCM(1)
	cell = row.AddCell()
	cell.Value = "姓名"
	cell = row.AddCell()
	cell.Value = "年龄"

	row1 = sheet.AddRow()
	row1.SetHeightCM(1)
	cell = row1.AddCell()
	cell.Value = "狗子"
	cell = row1.AddCell()
	cell.Value = "18"

	row2 = sheet.AddRow()
	row2.SetHeightCM(1)
	cell = row2.AddCell()
	cell.Value = "蛋子"
	cell = row2.AddCell()
	cell.Value = "28"

	err = file.Save("./test_write.xlsx")
	if err != nil {
		fmt.Printf(err.Error())
	}
	http.ServeFile(w, r, "./test_write.xlsx")
}

func main() {
	http.HandleFunc("/", excel)               //设置访问路由
	http.HandleFunc("/down", downExcel)       //设置访问路由
	err := http.ListenAndServe(":21000", nil) //设置监听端口
	if err != nil {
		fmt.Println("server error:", err)
	}
}
